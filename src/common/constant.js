const QuestionList = [
  {
      _id: 1,
      question: "At a party do you:",
      answers: ["Interact with many, including strangers", "Interact with a few, known to you"]
  },
  {
      _id: 2,
      question: "Are you more:",
      answers: ["Realistic than speculative", "Speculative than realistic"]
  },
  {
      _id: 3,
      question: "Is it worse to:",
      answers: ["Have your “head in the clouds”", "Be “in a rut”"]
  },
  {
      _id: 4,
      question: "Are you more impressed by:",
      answers: ["Principles", "Emotions"]
  },
  {
      _id: 5,
      question: "Are more drawn toward the:",
      answers: ["Convincing", "Touching"]
  },
  {
      _id: 6,
      question: "Do you prefer to work:",
      answers: ["To deadlines", "Just “whenever”"]
  },
  {
      _id: 7,
      question: "Do you tend to choose:",
      answers: ["Rather carefully", "Somewhat impulsively"]
  },
  {
      _id: 8,
      question: "At parties do you:",
      answers: ["Stay late, with increasing energy", "Leave early with decreased energy"]
  },
  {
      _id: 9,
      question: "Are you more attracted to:",
      answers: ["Sensible people", "Imaginative people"]
  },
  {
      _id: 10,
      question: "Are you more interested in:",
      answers: ["What is actual", "What is possible"]
  },
  {
    _id: 11,
    question: "In judging others are you more swayed by:",
    answers: ["Laws than circumstances", "Circumstances than laws"]
  },
  {
      _id: 12,
      question: "In approaching others is your inclination to be",
      answers: ["Objective", "Personal"]
  },
  {
      _id: 13,
      question: "Are you more:",
      answers: ["Punctual", "Leisurely"]
  },
  {
      _id: 14,
      question: "Does it bother you more having things:",
      answers: ["Incomplete", "Completed"]
  },
  {
      _id: 15,
      question: ". In your social groups do you:",
      answers: ["Keep abreast of other’s happenings", "Get behind on the news"]
  },
  {
      _id: 16,
      question: "In doing ordinary things are you more likely to:",
      answers: ["Do it the usual way", "Do it your own way"]
  },
  {
      _id: 17,
      question: "Writers should:",
      answers: ["Say what they mean and mean what they say", "Express things more by use of analogy"]
  },
  {
      _id: 18,
      question: "Which appeals to you more:",
      answers: ["Consistency of thought", "Harmonious human relationships"]
  },
  {
      _id: 19,
      question: "Are you more comfortable in making:",
      answers: ["Logical judgments", "Value judgments"]
  },
  {
      _id: 20,
      question: "Do you want things:",
      answers: ["Settled and decided", "Unsettled and undecided"]
  },
  {
    _id: 21,
    question: "Would you say you are more:",
    answers: ["Serious and determined", "Easy-going"]
  },
  {
      _id: 22,
      question: "In phoning do you: ",
      answers: ["Rarely question that it will all be said", "Rehearse what you’ll say"]
  },
  {
      _id: 23,
      question: "Facts:",
      answers: ["Speak for themselves", "Illustrate principles"]
  },
  {
      _id: 24,
      question: "Are visionaries:",
      answers: ["somewhat annoying", "rather fascinating"]
  },
  {
      _id: 25,
      question: "Are you more often:",
      answers: ["a cool-headed person", "a warm-hearted person"]
  },
  {
      _id: 26,
      question: "Is it worse to be:",
      answers: ["unjust", "merciless"]
  },
  {
      _id: 27,
      question: "Should one usually let events occur:",
      answers: ["by careful selection and choice", "randomly and by chance"]
  },
  {
      _id: 28,
      question: "Do you feel better about:",
      answers: ["having purchased", "having the option to buy"]
  },
  {
      _id: 29,
      question: "In company do you:",
      answers: ["initiate conversation", "wait to be approached"]
  },
  {
      _id: 30,
      question: "Common sense is:",
      answers: ["rarely questionable", "frequently questionable"]
  },
  {
    _id: 31,
    question: "Children often do not:",
    answers: ["make themselves useful enough", "exercise their fantasy enough"]
  },
  {
      _id: 32,
      question: ". In making decisions do you feel more comfortable with:",
      answers: ["standards", "feelings"]
  },
  {
      _id: 33,
      question: "Are you more:",
      answers: ["firm than gentle", "gentle than firm"]
  },
  {
      _id: 34,
      question: "Which is more admirable:",
      answers: ["the ability to organize and be methodical", "the ability to adapt and make do"]
  },
  {
      _id: 35,
      question: "Do you put more value on:",
      answers: ["infinite", "open-minded"]
  },
  {
      _id: 36,
      question: ". Does new and non-routine interaction with others: ",
      answers: ["stimulate and energize you", "tax your reserves"]
  },
  {
      _id: 37,
      question: "Are you more frequently:",
      answers: ["a practical sort of person", "a fanciful sort of person"]
  },
  {
      _id: 38,
      question: "Are you more likely to:",
      answers: ["see how others are useful", "see how others see"]
  },
  {
      _id: 39,
      question: "Which is more satisfying:",
      answers: ["to discuss an issue thoroughly", "to arrive at agreement on an issue"]
  },
  {
      _id: 40,
      question: "Which rules you more:",
      answers: ["your head", "your heart"]
  },
  {
    _id: 41,
    question: "Are you more comfortable with work that is:",
    answers: ["contracted", "done on a casual basis"]
  },
  {
      _id: 42,
      question: "Do you tend to look for:",
      answers: ["the orderly", "whatever turns up"]
  },
  {
      _id: 43,
      question: "Do you prefer:",
      answers: [". many friends with brief contact", "a few friends with more lengthy contact"]
  },
  {
      _id: 44,
      question: "Do you go more by:",
      answers: ["facts", "principles"]
  },
  {
      _id: 45,
      question: "Are you more interested in:",
      answers: ["production and distribution", "design and research"]
  },
  {
      _id: 46,
      question: "Which is more of a compliment:",
      answers: ["There is a very logical person.", "There is a very sentimental person."]
  },
  {
      _id: 47,
      question: "Do you value in yourself more that you are:",
      answers: ["unwavering", "devoted"]
  },
  {
      _id: 48,
      question: "Do you more often prefer the",
      answers: ["final and unalterable statement", "tentative and preliminary statement"]
  },
  {
      _id: 49,
      question: "Are you more comfortable:",
      answers: ["after a decision", "before a decision"]
  },
  {
      _id: 50,
      question: "Do you:",
      answers: ["speak easily and at length with strangers", "find little to say to strangers"]
  },
  {
    _id: 51,
    question: "Are you more likely to trust your:",
    answers: ["experience", "hunch"]
  },
  {
      _id: 52,
      question: "Do you feel:",
      answers: ["more practical than ingenious", "more ingenious than practical"]
  },
  {
      _id: 53,
      question: "Which person is more to be complimented one of",
      answers: ["clear reason", "strong feeling"]
  },
  {
      _id: 54,
      question: "Are you inclined more to be:",
      answers: ["fair-minded", "sympathetic"]
  },
  {
      _id: 55,
      question: "Is it preferable mostly to:",
      answers: ["make sure things are arranged", "just let things happen"]
  },
  {
      _id: 56,
      question: "In relationships should most things be:",
      answers: ["re-negotiable", "random and circumstantial "]
  },
  {
      _id: 57,
      question: "When the phone rings do you:",
      answers: ["hasten to get to it first", "hope someone else will answer"]
  },
  {
      _id: 58,
      question: ". Do you prize more in yourself:",
      answers: ["a strong sense of reality", "a vivid imagination"]
  },
  {
      _id: 59,
      question: "Are you drawn more to:",
      answers: ["fundamentals", "overtones"]
  },
  {
      _id: 60,
      question: "Which seems the greater error:",
      answers: ["to be too passionate", "to be too objective"]
  },
  {
    _id: 61,
    question: "Do you see yourself as basically:",
    answers: ["hard-headed", "soft-hearted"]
  },
  {
      _id: 62,
      question: "Which situation appeals to you more:",
      answers: ["the structured and scheduled", "the unstructured and unscheduled"]
  },
  {
      _id: 63,
      question: "Are you a person that is more:",
      answers: ["Routinized than whimsical", "Whimsical than routinized"]
  },
  {
      _id: 64,
      question: "Are you more inclined to be:",
      answers: ["easy to approach", "somewhat reserved"]
  },
  {
      _id: 65,
      question: "In writings do you prefer:",
      answers: ["the more literal", "the more figurative"]
  },
  {
      _id: 66,
      question: ". Is it harder for you to:",
      answers: ["identify with others", "utilize others"]
  },
  {
      _id: 67,
      question: "Which do you wish more for yourself:",
      answers: ["clarity of reason", "strength of compassion"]
  },
  {
      _id: 68,
      question: "Which is the greater fault:",
      answers: ["being indiscriminate", "being critical"]
  },
  {
      _id: 69,
      question: "Do you prefer the:",
      answers: ["planned event", "unplanned event "]
  },
  {
      _id: 70,
      question: "Do you tend to be more:",
      answers: ["deliberate than spontaneous", "spontaneous than deliberate"]
  },
];

const ResultList = [
  
];

export default QuestionList;