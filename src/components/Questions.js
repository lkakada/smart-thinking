import React, { useState } from 'react';
import styled from 'styled-components';
import QuestionList from '../common/constant';

const Container = styled.div`
    display: flex;
    flex-wrap: wrap;
    max-height: 33rem;
    overflow-y: scroll;
    padding: 1rem 2rem;
    font-size: 14px;
`;

const Item = styled.div`
    width: 50%;
`;

const Questions = () => {
    const [answer, setAnswer] = useState([]);
    const handleOption = (evt, questionId) => {
        const targetValue = evt.target.value;
        let updateAnswer = answer;
        updateAnswer[questionId - 1] = targetValue;
        setAnswer([...updateAnswer]);
        console.log('updateAnswer=>', updateAnswer);
    }
    return (
        <Container>
            {QuestionList.map((question) => (
                <Item key={question._id}>
                    <h4>{question._id}. {question.question}</h4>
                    <ol type="a">
                        <li >
                            <label>
                                <input
                                    type="radio"
                                    value={question._id - 1}
                                    checked={answer[question._id - 1] === question.answers[0]}
                                    onChange={(evt, questionId) => handleOption(evt, question._id)}
                                />
                                {question.answers[0]}
                            </label>
                        </li>
                        <li >
                            <label>
                                <input
                                    type="radio"
                                    value={question._id}
                                    checked={answer[question._id - 1] === question.answers[1]}
                                    onChange={(evt, questionId) => handleOption(evt, question._id)}
                                />
                                {question.answers[1]}
                            </label>
                        </li>
                    </ol>
                </Item>
            ))}
        </Container>
    )
};

export default Questions;