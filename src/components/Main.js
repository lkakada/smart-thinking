import Questions from './Questions';
import Result from './Result';
import Scoring from './Scoring';
import styled from 'styled-components';
const Container = styled.div`
    display: flex;
`;
const ItemQuestion = styled.div`
    width: 50%;
    background-color: lightblue;
`;
const ItemScoring= styled.div`
    width: 50%;
    background-color: lightgray;
`;
const ContainerResult = styled.div`
    background-color: lightgreen;
    text-align: center;
`;
const Main = () => {
    return (
        <>
            <Container>
                <ItemQuestion>
                    <Questions />
                </ItemQuestion>
                <ItemScoring>
                    <Scoring />
                </ItemScoring>
            </Container>
            <ContainerResult>
                <Result />
            </ContainerResult>
        </>
    )
};

export default Main;