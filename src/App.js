import Main from './components/Main';

const App = () => {
  return (
    <>
      <h1>Smart thinking</h1>
      <Main />
    </>
  )
}

export default App;
